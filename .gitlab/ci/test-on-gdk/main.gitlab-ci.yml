include:
  - local: .gitlab/ci/qa-common/main.gitlab-ci.yml
  - local: .gitlab/ci/qa-common/rules.gitlab-ci.yml
  - local: .gitlab/ci/qa-common/variables.gitlab-ci.yml

.run-tests:
  stage: test
  image: ${REGISTRY_HOST}/${REGISTRY_GROUP}/gitlab-build-images/debian-bullseye-ruby-${RUBY_VERSION}:bundler-2.3-chrome-${CHROME_VERSION}-docker-${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  tags:
    - e2e
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - sysctl -n -w fs.inotify.max_user_watches=524288
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    QA_GDK_IMAGE: "${CI_REGISTRY}/${CI_PROJECT_PATH}/gitlab-qa-gdk:master"
    QA_GENERATE_ALLURE_REPORT: "true"
    QA_CAN_TEST_PRAEFECT: "false"
    QA_INTERCEPT_REQUESTS: "false"
    TEST_LICENSE_MODE: $QA_TEST_LICENSE_MODE
    EE_LICENSE: $QA_EE_LICENSE
    GITHUB_ACCESS_TOKEN: $QA_GITHUB_ACCESS_TOKEN
    GITLAB_QA_ADMIN_ACCESS_TOKEN: $QA_ADMIN_ACCESS_TOKEN
    RSPEC_REPORT_OPTS: "--format QA::Support::JsonFormatter --out tmp/rspec-${CI_JOB_ID}.json --format RspecJunitFormatter --out tmp/rspec-${CI_JOB_ID}.xml --format html --out tmp/rspec-${CI_JOB_ID}.htm --color --format documentation"
  timeout: 2 hours
  artifacts:
    when: always
    paths:
      - test_output
      - logs
    expire_in: 7 days
    reports:
      junit: test_output/**/rspec-*.xml
      dotenv: suite_status.env
  script:
    - echo -e "\e[0Ksection_start:`date +%s`:pull_image\r\e[0KPull GDK QA image"
    - docker pull ${QA_GDK_IMAGE}
    - echo -e "\e[0Ksection_end:`date +%s`:pull_image\r\e[0K"
    - echo -e "\e[0Ksection_start:`date +%s`:launch_gdk_and_tests\r\e[0KLaunch GDK and run QA tests"
    - cd qa && bundle install --jobs=$(nproc) --retry=3 --quiet
    - mkdir -p $CI_PROJECT_DIR/test_output $CI_PROJECT_DIR/logs/gdk $CI_PROJECT_DIR/logs/gitlab
    # This command matches the permissions of the user that runs GDK inside the container.
    - chown -R 1000:1000 $CI_PROJECT_DIR/test_output $CI_PROJECT_DIR/logs $CI_PROJECT_DIR/qa/knapsack
    - |
      docker run --rm --name gdk --add-host gdk.test:127.0.0.1 --shm-size=2gb \
        --env-file <(bundle exec rake ci:env_var_name_list) \
        --volume /var/run/docker.sock:/var/run/docker.sock:z \
        --volume $CI_PROJECT_DIR/test_output:/home/gdk/gdk/gitlab/qa/tmp:z \
        --volume $CI_PROJECT_DIR/logs/gdk:/home/gdk/gdk/log \
        --volume $CI_PROJECT_DIR/logs/gitlab:/home/gdk/gdk/gitlab/log \
        --volume $CI_PROJECT_DIR/qa/knapsack:/home/gdk/gdk/gitlab/qa/knapsack \
        ${QA_GDK_IMAGE} "${CI_COMMIT_SHA}" "$RSPEC_REPORT_OPTS $TEST_GDK_TAGS --tag ~requires_praefect"
    # The above image's launch script takes two arguments only - first one is the commit sha and the second one Rspec Args
  allow_failure: true
  after_script:
    - |
      if [ "$CI_JOB_STATUS" == "failed" ]; then
        echo "SUITE_FAILED=true" >> suite_status.env
      fi

download-knapsack-report:
  extends:
    - .download-knapsack-report
    - .rules:download-knapsack

test-on-gdk-smoke:
  extends:
    - .run-tests
  parallel: 2
  variables:
    TEST_GDK_TAGS: "--tag smoke"
  rules:
    - when: always

test-on-gdk-full:
  extends:
    - .run-tests
  parallel: 5
  rules:
    - when: manual

# ==========================================
# Post test stage
# ==========================================
e2e-test-report:
  extends:
    - .e2e-test-report
    - .rules:report:allure-report
  variables:
    ALLURE_RESULTS_GLOB: test_output/allure-results

upload-knapsack-report:
  extends:
    - .upload-knapsack-report
    - .rules:report:process-results
  variables:
    QA_KNAPSACK_REPORT_FILE_PATTERN: $CI_PROJECT_DIR/test_output/knapsack/*/*.json

export-test-metrics:
  extends:
    - .export-test-metrics
    - .rules:report:process-results
  variables:
    QA_METRICS_REPORT_FILE_PATTERN: $CI_PROJECT_DIR/test_output/test-metrics-*.json

relate-test-failures:
  extends:
    - .relate-test-failures
    - .rules:report:process-results
  variables:
    QA_RSPEC_JSON_FILE_PATTERN: $CI_PROJECT_DIR/test_output/rspec-*.json

generate-test-session:
  extends:
    - .generate-test-session
    - .rules:report:process-results
  variables:
    QA_RSPEC_JSON_FILE_PATTERN: $CI_PROJECT_DIR/test_output/rspec-*.json

notify-slack:
  extends:
    - .notify-slack
    - .rules:report:process-results
  variables:
    QA_RSPEC_XML_FILE_PATTERN: $CI_PROJECT_DIR/test_output/rspec-*.xml
